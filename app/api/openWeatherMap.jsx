var axios=require('axios');

const OPEN_WEATHER_URL = 'http://api.openweathermap.org/data/2.5/weather?appid=46525936078689ee230dad1b6f8c42fb&units=metric&lang=ru';

module.exports = {
    getWeather: function(location) {
        let encodedLocation = encodeURI(location);
        let requestUrl = `${OPEN_WEATHER_URL}&q=${encodedLocation}`;
        debugger;
        return axios.get(requestUrl).then(function(json) {
            debugger;
            if (json.data.cod && json.data.message) {
               throw new Error(json.data.message)
            } else {
                return json.data;
            }
        }, function(error){
            debugger;
            throw new Error(error.message)
        });
    }
}