/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var React = require('react');
var WeatherForm = require('WeatherForm');
var WeatherMessage = require('WeatherMessage');
var openWeatherMap = require('openWeatherMap');

var Weather = React.createClass({
    getInitialState: function() {
        return {
            isLoading:false
        }
    },
    handleUpdate: function(location){
        var self = this;
        this.setState({isLoading:true});
        openWeatherMap.getWeather(location).then(function(weather){
            self.setState({
                isLoading:false,
                location:location,
                weather:weather.main.temp
            });
        },function(errMsg){
            self.setState({
                isLoading:false,
                location:false,
                weather:false                
            });
            alert(errMsg);
        });
    },
    render: function () {
        var {isLoading, location, weather} = this.state;
        
        function renderMessage() {
            if(isLoading) {
                return <h3>Fetching weather</h3>
            } else if(location && weather) {
                return  <WeatherMessage location={location} weather={weather}/>
            }
        }
        
        return (
                <div>
                    <h1>Get Weather</h1>
                    <WeatherForm onUpdate={this.handleUpdate}/>
                    {renderMessage()}
                </div>
                );
    }
});
module.exports = Weather;