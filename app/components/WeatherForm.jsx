var React = require('react');
var WeatherForm = React.createClass({
    onFormSubmit:  function(e) {
        e.preventDefault();
        let location = this.refs.location.value;
        if(location.length > 2) {
            this.refs.location.value = '';
            this.props.onUpdate(location);
        }
    },
    render: function() {
        return (
                <form onSubmit={this.onFormSubmit}>
                    <input type="text" placeholder="Город..." ref="location"/>
                    <button>Get Weather</button>
                </form>
                );
    }
});
module.exports = WeatherForm;