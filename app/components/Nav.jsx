/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var React = require('react');
var {Link, IndexLink} = require('react-router');
var Nav = React.createClass({
    render: function () {
        return (
                <nav>
                    <IndexLink to="/" activeClassName="active">Погода</IndexLink>
                    <Link to="/about" activeClassName="active">О программе</Link>
                    <Link to="/examples" activeClassName="active">Примеры</Link>
                </nav>
                );
    }
});
module.exports = Nav;